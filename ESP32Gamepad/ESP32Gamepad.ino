// Need ESP32-BLE-Gamepad library
#include <BleGamepad.h>

BleGamepad bleGamepad;

#define PIN_HOVER 25
#define PIN_XAXIS 26
#define PIN_YAXIS 27
int buttonPins[] = {17, 16, 4, 14, 33};

int hover = 0;
int xAxis = 0;
int yAxis = 0;

bool buttonState[5];

void setup()
{
  Serial.begin(115200);
  
  Serial.println("Setting PinModes");

  pinMode(PIN_HOVER, INPUT);
  pinMode(PIN_XAXIS, INPUT);
  pinMode(PIN_YAXIS, INPUT);

  for (int i = 0; i < 5; i++) {
    pinMode(buttonPins[i], INPUT_PULLUP);
  }

  Serial.println("Initiating BLE");
  bleGamepad.begin();
}

void loop()
{
  if (bleGamepad.isConnected())
  {
    hover = analogRead(PIN_HOVER);
    hover = map(hover, 4095, 0, -32767, 32767);

    xAxis = analogRead(PIN_XAXIS);
    xAxis = map(xAxis, 0, 4095, -32767, 32767);

    yAxis = analogRead(PIN_YAXIS);
    yAxis = map(yAxis, 0, 4095, -32767, 32767);

    bleGamepad.setAxes(xAxis, yAxis, hover, 0, 0, 0, 0, 0, DPAD_CENTERED);

    for (int i = 0; i < 5; i++) {
      if (!digitalRead(buttonPins[i])) {
        if (!buttonState[i]) {
          buttonState[i] = 1;
          bleGamepad.press(i + 1);
        }
      } else {
        if (buttonState[i]) {
          buttonState[i] = 0;
          bleGamepad.release(i + 1);
        }
      }
    }
  }
  delay(10);
}
